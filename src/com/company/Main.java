package com.company;

public class Main {

    public static void main(String[] args) {
        int a = 4, b = 2, c = 3;
        double SumSquare = 0;
        if ((a >= b) && (b >= c)) {
            SumSquare = a * a + b * b;
        } else if ((b >= a) && (c >= a)) {
            SumSquare = b * b + c * c;
        } else if ((a >= b) && (c >= b)) {
            SumSquare = a * a + c * c;
        }
        System.out.println("Сумма квадратов: " + SumSquare);
    }
}
